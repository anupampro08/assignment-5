package com.assign;
import java.sql.*;
import java.util.Scanner;

public class StudentUpdate {
   static final String jdbc = "oracle.jdbc.driver.OracleDriver";
   static final String url = "jdbc:oracle:thin:@localhost:1521:orcl";
   
   static String user = "sys as sysdba";
   static String pass = "Oracle@1234";

   public static void main(String[] args) {
      Connection conn = null;
      Statement stmt = null;
      Scanner sc = new Scanner(System.in);

      try {
         Class.forName(jdbc);

         System.out.println("Connecting to database...");
         conn = DriverManager.getConnection(url, user, pass);

         System.out.println("Creating statement...");
         stmt = conn.createStatement();

         System.out.print("Enter the ID of the record to update: ");
         int id = sc.nextInt();
         sc.nextLine();

         System.out.print("Enter the updated name: ");
         String name = sc.nextLine();

         String sql = "UPDATE STUDENT SET STUDENT_NAME = '" + name + "' WHERE STUDENT_ID = " + id;
         stmt.executeUpdate(sql);

         System.out.println("Record updated successfully.");

         stmt.close();
         conn.close();
      } catch (SQLException se) {
         se.printStackTrace();
      } catch (Exception e) {
         e.printStackTrace();
      } 
   }
}
