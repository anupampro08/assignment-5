package com.assign;

import java.sql.*;
import java.util.Scanner;

public class StudentInsert {
   static final String jdbc = "oracle.jdbc.driver.OracleDriver";
   static final String url = "jdbc:oracle:thin:@localhost:1521:orcl";

   static String user = "sys as sysdba";
   static String pass = "Oracle@1234";

   public static void main(String[] args) {
      Connection conn = null;
      Statement stmt = null;
      Scanner sc = new Scanner(System.in);
      String continueInsert = "yes";

      try {
         Class.forName(jdbc);

         System.out.println("Connecting to database...");
         conn = DriverManager.getConnection(url, user, pass);

         System.out.println("Connection Successful");
         stmt = conn.createStatement();

         while (continueInsert.equalsIgnoreCase("yes")) {
            System.out.print("Enter student name: ");
            String name = sc.next();

            System.out.print("Enter student course id: ");
            int courseId = sc.nextInt();
            sc.nextLine();

            System.out.print("Enter student address: ");
            String address = sc.nextLine();

            System.out.print("Enter student phone: ");
            String phone = sc.nextLine();

            String sql = "INSERT INTO STUDENT (STUDENT_NAME, COURSE_ID, ADDRESS, PHONE) VALUES ('" + name + "', " + courseId + ", '" + address + "', '" + phone + "')";
            stmt.executeUpdate(sql);

            System.out.println("Record inserted successfully.");
            System.out.print("Do you want to continue inserting records (yes/no)? ");
            continueInsert = sc.nextLine();
         }

         stmt.close();
         conn.close();
      } catch (SQLException se) {
         se.printStackTrace();
      } catch (Exception e) {
         e.printStackTrace();
      } 
   }
}
